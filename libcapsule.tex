\documentclass[xcolor={svgnames},aspectratio=169]{beamer}
\usetheme{Collabora}

%\usepackage{color}
\usepackage{graphicx}
\usepackage{placeins}

\title{libcapsule - Segregated Dynamic Linking}
\author{Vivek Das Mohapatra}

\usepackage{pgfpages}
\setbeameroption{show notes on second screen}

\begin{document}

\maketitle

\section{Introduction}

\begin{frame}
  \frametitle{The Problem}

  \begin{itemize}
    \item{Applications ‘Containerised’}
    \item{Libraries \only<2->{\emph{mostly} }come from a runtime}
    \item<2->{…but \emph{some} still need to come from the host}
      \begin{itemize}
        \item<3->{notably mesa (libGL \& friends)}
      \end{itemize}
    \item<4->{host and runtime libraries may be incompatible}
  \end{itemize}

\note{
  \begin{itemize}
  \item{The hot new way to deliver applications/games/etc is in a container or runtime environment}
  \item{The runtime provides the libraries you need}
  \item<2->{… Actually, that is a not completely true}
  \item<3->{They mostly come from the runtime}
  \item<4->{Some are tied to hw so must come from the host}
  \end{itemize}
}
\end{frame}

\begin{frame}
  \frametitle{What does the problem look like?}
  \input{vanilla-dynamic-link-diagram.tex}

\note{
  \begin{itemize}
  \item{Here’s a simple example}
  \item{libRUNT and libHOST both declare they need libDEP}
  \item{… but libHOST is not compatible with the runtime’s libDEP}
  \item{linker will only load one instance of libDEP: It considers the dependency met once it has that copy}
  \end{itemize}
}
\end{frame}

\begin{frame}
  \frametitle{What could a solution look like?}
  \input{segregated-dynamic-link-diagram.tex}

\note{
  \begin{itemize}
  \item{Here we can see a segregated link setup}
  \item{The application can see all runtime libraries}
  \item{The application \& runtime libs can see each other \& libHOST}
  \item{But host libDEP can \emph{only} be seen by libHOST}
  \end{itemize}
}
\end{frame}

\begin{frame}
  \frametitle{Objectives}
  \begin{itemize}
    \item{Expose only the library we want to isolate}
      \begin{itemize}
      \item{its dependencies \emph{not} exposed to us}
      \end{itemize}
    \item<2->{No code changes}
    \item<3->{No recompilation}
    \item<4->{No performance hit}
    \item<5->{Transparent use of nonstandard search-path}
    \item<6->{Minimal manual intervention}
  \end{itemize}

  \note<6->{
    \begin{itemize}
      \item{What do we mean by minimal intervention? \\
    In order of preference:}
  \begin{itemize}
    \item{Purely runtime isolation mechanism}
    \item{Some compilation required but basically automatic}
    \item{Manual intervention required to generate the isolating ‘thing’}
    \end{itemize}
  \end{itemize}
}
\end{frame}

\section{The Pieces of the Puzzle}

\begin{frame}
  \frametitle{1: Private Dependencies}
  \vspace*{-3em}
  \begin{itemize}
  \item{Library should have isolated dependencies}
  \item<2->{Normally all dependencies in a single linked list}
  \item<3->{So how do we do this?}
  \end{itemize}
  \uncover<2>{\input{link-map-diagram.tex}}

\end{frame}

\begin{frame}
  \frametitle{dlmopen()}
  \begin{itemize}
  \item{The answer is \texttt{dlmopen()}}
    \begin{itemize}
    \item{like \texttt{dlopen()} but \emph{can} create a new link map}
    \item{… or can add a new entry to an existing link map}
    \item{more-or-less workable from at least glibc 2.19}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{2: Picking the right libraries}
  \begin{itemize}
  \item{\texttt{dlmopen()} automatically loads dependencies}
  \item<2->{Libraries are picked from the search path}
  \item<3->{First match wins}
  \item<4->{Isolated libraries should come from a non-standard search path}
    \begin{itemize}
      \item<4->{Must divert searches for dependencies of isolated libraries}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Controlling the link map}
  \begin{itemize}
  \item{Linker loads all listed dependencies}
  \item<2->{The linker \emph{won't} reload items already in the map}
    \begin{itemize}
    \item<3->{Loading libraries explicitly (by full path)}
    \item<3->{In reverse dependency order}
    \item<3->{We can control exactly what gets linked}
    \end{itemize}
  \end{itemize}
\note{
Effectively we populat the link map by hand - by doing dependency resolution
by hand we prevent the linke's automatic searching from kicking in: A classic
convenience \emph{vs} control trade-off
}
\end{frame}

\begin{frame}
  \frametitle{3: Automatically exposing symbols}
  \begin{itemize}
    \item<1->{Isolated the \texttt{dlmopen()}ed symbols completely}
    \item<2->{Need callers to get to them automatically}
    \item<3->{Need to understand dynamic library calls}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Jumping to a foreign function}
\vspace*{-1em}
\input{foreign-function-call-uml-diagram-A.tex}
\note{
\begin{itemize}[before=\normalsize,itemsep=0.2\baselineskip]
  \ffevery{Calling code puts foreign function arguments on the stack}
  \ffevery{Excution jumps to a fixed offset in the PLT (specific to this function)}
  \ffevery{The PLT stub looks up the corresponding address in the RR} and jumps to it
\end{itemize}
}
\end{frame}

\begin{frame}
\frametitle{First call - resolving the address}
\input{foreign-function-call-uml-diagram-B.tex}
\note{
\begin{itemize}[before=\normalsize,itemsep=0.2\baselineskip]
  \ffmaybe{The fixup code pointed at by the RR asks the linker for the real address}
  \ffmaybe{The linker searches the calling DSO dependencies for the symbol}
  \ffmaybe{The fixup code writes the address into the RR slot}
  \ffmaybe{The fixup code jumps to the address in the RR slot}
\end{itemize}
}
\end{frame}

\begin{frame}
\frametitle{The foreign function call}
\input{foreign-function-call-uml-diagram-C.tex}
\note{
\begin{itemize}[before=\normalsize,itemsep=0.2\baselineskip]
  \ffevery{Jump to funcX}
  \ffevery{The code in the foreign DSO pulls the arguments off the stack}
  \ffevery{Function does whatver it does}
  \ffevery{The return value is pushed onto the stack}
  \ffevery{Execution jumps back to the caller}
\end{itemize}
}

\end{frame}

\begin{frame}
  \frametitle{Control the RR, Control the call…}
  \vspace*{-3em}
  \begin{itemize}
  \item<1->{If we scribble on the RR slot before the first call:}
    \begin{itemize}
    \item<2->{The PLT fixup will never be invoked}
    \item<3->{The linker never resolves the symbol’s address}
    \item<4->{Total control over where the function call goes}
    \item<5->{At no point do we need to know the function signature}
    \end{itemize}
  \item<6->{Key question — can we find the RR?}
    \begin{itemize}
    \item<7->{Yes — we can!}
    \item<8->{The link map → to ELF data for each library}
    \item<9->{libelf can interrogate this}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Putting the Pieces Together}
  \begin{itemize}
    \item<1->{Make a shim library with the target’s \textbf{soname}}
    \item<1->{Put the shim on the search path}
    \item<2->{During the shim’s init:}
      \begin{itemize}
      \item<3->{\texttt{dlmopen()} the real library and its dependencies}
        \begin{itemize}
          \item<4->{Do this in reverse dependency order}
          \item<5->{Search the alternate library path}
        \end{itemize}
      \item<6->{Walk the link map \& scribble on all the RRs}
      \end{itemize}
  \end{itemize}
\note{
  Needs the list of exported symbols, but not their signatures \\
  Needs to know its target’s soname \\
  Otherwise fully automated
}
\end{frame}

\section{These Yaks Aren’t Shaving Themselves}

\begin{frame}
  \frametitle{Some Terminology}
  \begin{itemize}
    \item{Call an isolated set of libraries a \emph{capsule}}
    \item{Assume they come from an fs mounted at \textbf{/host}}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{dlopen() in capsules}
  \begin{itemize}
  \item{\texttt{dlopen()} can’t be called from inside a capsule}
    \uncover<2->{%
      \begin{itemize}
      \item{replace capsule’s dlopen with a wrapper that calls \texttt{dlmopen()}}
      \item{remap paths to \textbf{/host} in the wrapper}
      \item{\texttt{dlmopen()} doesn’t accept RTLD\_GLOBAL}
      \end{itemize}
    }
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{dlsym() now has a split personality}
  \begin{itemize}
  \item{\texttt{dlsym()} outside the capsule has to do extra work}
      \begin{itemize}
      \item<2->{Transparently search the hidden capsule dl handle}
      \item<3->{\texttt{dlsym()} outside capsule should find inner symbols}
        \begin{itemize}
        \item<3->{but only symbols from the direct target of libcapsule}
        \item<3->{\emph{not} symbols from that target’s dependencies}
        \end{itemize}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{dlopen() outside capsules}
  \begin{itemize}
  \item{\texttt{dlopen()} outside capsule must trigger RR scribbling}
      \begin{itemize}
      \item<2->{replace external \texttt{dlopen()} with a wrapper that does this}
      \item<3->{open new DSO → automatically divert its RRs into the capsule}
      \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Extra Problems}
  \begin{itemize}
  \item{Currently each namespace has its own glibc copy}
  \item<2->{This makes anything touching threads very deadlock-prone}
  \item<3->{*\texttt{alloc()}/\texttt{free()} pairing is a problem}
      \begin{itemize}
      \item<4->{Propose RTLD\_\emph{SHARED}}
      \item<4->{Currently being reviewd by glibc upstream}
      \item<5->{For now, replace *\texttt{alloc()}/\texttt{free()} cluster inside the capsule}
      \end{itemize}
  \end{itemize}
\end{frame}

\section{And Finally}

\begin{frame}
  \frametitle{*drumroll*}
  \begin{itemize}
    \item{Does it \emph{actually} work?}
    \item<2->{Yes!}
      \begin{itemize}
        \item<3->{glxinfo et al}
        \item<3->{openarena (SDL 1 \& 2)}
        \item<3->{Dungeon Defenders (SDL 2)*}
        \item<3->{Floating Point (Simple Unity game)}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Any Questions… ?}
  \begin{itemize}
  \item{The code: \url{https://gitlab.collabora.com/vivek/libcapsule}}
  \item{This talk: \url{https://gitlab.collabora.com/vivek/libcapsule-talk}}
  \end{itemize}
\end{frame}

\end{document}
